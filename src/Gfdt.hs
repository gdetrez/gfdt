{-# LANGUAGE QuasiQuotes, MultiWayIf #-}
module Gfdt where

import Data.Char (toLower)
import Data.Maybe (fromJust)
import System.Exit
import PGF
import System.Console.Docopt
import System.FilePath (takeExtension)
import Text.Pandoc.Definition

import Translator

patterns :: Docopt
patterns = [docopt|gfdt 0.1.0
  Translate formatted documents using gf

Usage:
  gfdt --help
  gfdt --pgf=PGF --from=LANG --to=LANG

Options:
  -h, --help        Show usage
  -p, --pgf=PGF     Translation grammar
  -f, --from=LANG   Source language
  -t, --to=LANG     Target language
|]

gfdt :: [String] -> Pandoc -> IO Pandoc
gfdt argv b = do
  args <- parseArgsOrExit patterns argv
  if | args `isPresent` longOption "help" -> do putStrLn (usage patterns)
                                                exitSuccess
     | args `isPresent` longOption "pgf" -> do
      let pgffile = fromJust (args `getArg` longOption "pgf")
          from    = fromJust (args `getArg` longOption "from")
          to      = fromJust (args `getArg` longOption "to")
      pgf <- readPGF pgffile
      let translator = makeGfTranslator pgf (mkCId from) (mkCId to)
      let b' = translate translator b
      return b'
     | otherwise -> do putStrLn (usage patterns)
                       exitFailure


-- From pandoc source code:
-- Determine default reader based on source file extensions
defaultReaderName :: String -> [FilePath] -> String
defaultReaderName fallback [] = fallback
defaultReaderName fallback (x:xs) =
  case takeExtension (map toLower x) of
    ".xhtml"    -> "html"
    ".html"     -> "html"
    ".htm"      -> "html"
    ".tex"      -> "latex"
    ".latex"    -> "latex"
    ".ltx"      -> "latex"
    ".rst"      -> "rst"
    ".org"      -> "org"
    ".lhs"      -> "markdown+lhs"
    ".db"       -> "docbook"
    ".opml"     -> "opml"
    ".wiki"     -> "mediawiki"
    ".dokuwiki" -> "dokuwiki"
    ".textile"  -> "textile"
    ".native"   -> "native"
    ".json"     -> "json"
    ".docx"     -> "docx"
    ".t2t"      -> "t2t"
    ".epub"     -> "epub"
    ".odt"      -> "odt"  -- so we get an "unknown reader" error
    ".pdf"      -> "pdf"  -- so we get an "unknown reader" error
    ".doc"      -> "doc"  -- so we get an "unknown reader" error
    _           -> defaultReaderName fallback xs

