module Translator where

import Control.Arrow (first)
import Data.Generics (everywhere, mkT)
import Data.Maybe (listToMaybe)
import Data.Monoid

import PGF
import Text.Pandoc.Definition hiding (Alignment)
import qualified Text.Pandoc.Builder as Pandoc

import Data.Alignment
import Destyle
import SmithWaterman

type Translator = String -> (String, Alignment)

makeGfTranslator :: PGF -> Language -> Language -> Translator
makeGfTranslator _ _ _ [] = ([], mempty)
makeGfTranslator pgf source target s =
    case listToMaybe (parse pgf source (startCat pgf) s) of
        Just t ->
          let (sourceTokens, targetTokens, gfAlignment) = PGF.alignment pgf (source, target) t
              alignment' = alignTranslation s (sourceTokens, targetTokens, gfAlignment)
              (targetStr, _) = unwordsWithAlignment targetTokens
          in (targetStr, alignment')
        Nothing -> ("PARSE ERROR: " ++ show s ++ "", fromList [])

alignTranslation :: String -> ([String], [String], [(Int,Int)]) -> Alignment
alignTranslation ss (st, tt, al) =
    al1 <> al2 <> al3 <> al4
  where (ss', al2') = unwordsWithAlignment st
        al2 = reverseAlignment al2'
        (_, al4) = unwordsWithAlignment tt
        al1 = align ss ss'
        al3 = fromList al


class Translatable a where
    translate :: Translator -> a -> a

instance Translatable Pandoc where
    translate t = everywhere (mkT (translateBlock t))

instance Translatable Block where
    translate t = everywhere (mkT (translateBlock t))

translateBlock :: Translator -> Block -> Block
translateBlock t b = case b of
    Plain is ->
        Plain (translateInlines t is)
    Para is ->
        Para (translateInlines t is)
    Header level attrs is ->
        Header level attrs (translateInlines t is)
    DefinitionList dl ->
        DefinitionList (map (first (translateInlines t)) dl)
    Table caption align' widths headers rows ->
        Table (translateInlines t caption) align' widths headers rows
    _ -> b

translateInlines :: Translator -> [Inline] -> [Inline]
translateInlines t content =
    let at = tokenize content
        (translation, alignment') = t (tokens at)
        at' = transferAnnotatedTokens at translation alignment'
    in restyle at'

instance Translatable a => Translatable (Pandoc.Many a) where
    translate t = Pandoc.fromList . map (translate t) . Pandoc.toList
