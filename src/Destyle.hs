{-# LANGUAGE RecordWildCards #-}
module Destyle where

import Control.Arrow
import Data.Foldable (foldMap)
import Data.List (findIndex)
import Data.Maybe (fromMaybe)
import Data.Monoid

import Text.Pandoc.Definition (Inline(..))

import Data.Alignment
import Text.Pandoc.Normalize

data AnnotatedTokens a = AT
    { tokens :: String, annotations :: [(Int, Int, a)] }
  deriving (Show, Eq)

-- | Concatenate two destyled values by offsetting the indices in the second.
concatAnnotatedTokens :: AnnotatedTokens a -> AnnotatedTokens a -> AnnotatedTokens a
concatAnnotatedTokens (AT strings1 styles1) (AT strings2 styles2) =
    AT (strings1 ++ strings2) (styles1 ++ style2')
  where
    style2' = map (offset (length strings1)) styles2
    offset :: Int -> (Int, Int, a) -> (Int, Int, a)
    offset n (i1,i2,x) = (i1+n, i2+n, x)

instance Monoid (AnnotatedTokens a) where
    mempty = AT [] []
    mappend = concatAnnotatedTokens

tokenize :: [Inline] -> AnnotatedTokens ([Inline] -> Inline)
tokenize = foldMap destyleone
  where
    destyleone (Str txt)    = AT txt []
    destyleone (Space)      = AT " " []
    destyleone (Emph is)    = apply Emph $ tokenize is
    destyleone (Strong is)    = apply Strong $ tokenize is
    destyleone (Strikeout is) = apply Strikeout $ tokenize is
    destyleone (Superscript is) = apply Superscript $ tokenize is
    destyleone (Subscript is) = apply Subscript $ tokenize is
    destyleone (Quoted s is) = apply (Quoted s) $ tokenize is
    destyleone (SmallCaps is) = apply SmallCaps $ tokenize is
    destyleone (Cite cs is) = apply (Cite cs) $ tokenize is
    destyleone (Image is image) = insert (Image is image)
    destyleone (Link is target) = apply (flip Link target) $ tokenize is
    destyleone code@(Code _ _)     = insert code
    destyleone math@(Math _ _)     = insert math
    destyleone inline = case inline of
        raw@(RawInline _ _) -> insert raw
        Span attrs is -> apply (Span attrs) $ tokenize is
        _            -> mempty
    apply :: a -> AnnotatedTokens a -> AnnotatedTokens a
    apply s (AT tokens as) = AT tokens ((0, length tokens - 1, s):as)
    insert _inline = AT [] [] -- [(0,0,const inline)]

restyle :: AnnotatedTokens ([Inline] -> Inline) -> [Inline]
restyle AT{..} = normalize $ map snd $ foldr apply (toInlineWithIndices tokens) annotations
  where
    apply :: (Int, Int, [Inline] -> Inline) -> [(Int, Inline)] -> [(Int,Inline)]
    apply (i1, i2, style) inlines =
        let ind1 = fromMaybe (length inlines) (findIndex ((==i1) . fst) inlines)
            ind2 = fromMaybe (length inlines) (findIndex ((==i2+1) . fst) inlines)
            ((pre, mid), post) = first (splitAt ind1) (splitAt ind2 inlines)
        in pre ++ [(i1,style (map snd mid))] ++ post

-- | Like words but returns the place of the token in the original string
toInlineWithIndices :: String -> [(Int, Inline)]
toInlineWithIndices = zip [0..] . map char2inline
  where
    char2inline c | c `elem` " \t\n"  = Space
                  | otherwise = Str [c]

-- | Given a string with annotation, a second string (e.g. a translation of the
-- first one) and a character alignment between the two, this builds a new
-- annotated string based on the later string, transfering the original
-- annotations according to the alignment.
transferAnnotatedTokens :: AnnotatedTokens a -> String -> Alignment -> AnnotatedTokens a
transferAnnotatedTokens (AT _s1 ano) s2 al = AT s2 ano' -- undefined -- AT s2 ano'
  where ano' = [ (i', j', x) | (i,j,x) <- ano, (i',j') <- alignRange al (i,j) ]
