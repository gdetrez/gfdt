module SmithWaterman where

import Control.Arrow (second)
import Data.Function (on)
import Data.List (maximumBy)
import Data.Monoid

import Data.Matrix hiding (fromList, trace)

import Data.Alignment hiding ((!), toList)

-- | Similarity function
sim :: Eq a => a -> a -> Int
sim token1 token2 | token1 == token2  = 4
                  | otherwise         = -1

showMatrix :: Show a => [[a]] -> String
showMatrix = unlines . map (unwords . map show)

align :: (Show a, Eq a) => [a] -> [a] -> Alignment
align s1 s2 =
    -- We get the matrix size here:
    let (rows, cols) = (length s1 + 1, length s2 + 1) in
    -- First, we "initialize" the matrix with undefined values for all cells
    let mI = matrix rows cols (\x -> error (show x)) :: Matrix (Int, [(Int,Int)])in
    -- Now we want to fill the matrix diagonally, i.e. start with
    -- (0,0) then (1,0), (0,1), (2,0), (1,1), (0,2), etc
    -- let's generate a list of such indices:
    let indices = [(j,1 + i - j) | i <- [1..2 * max rows cols], j <- [1..i], j <= rows, 1 + i - j <= cols ] in
    -- We can now fill in our matrice in the correct order:
    let mH = foldl updateElement mI indices in
    fromList (snd (hmax (Data.Matrix.toList mH)))
  where
    updateElement m (i,j) = setElem (h m (i,j)) (i,j) m
    h _ (i, j) | i < 1 || j < 1 = error "i and j cannot be < 0"
    h _ (1, _) = (0, [])
    h _ (_, 1) = (0, [])
    h m (i, j) = hmax
        [ (0, [])
        , let (x,t) = m ! (i-1, j-1) in (x + sim (s1 !! (i-2)) (s2 !! (j-2)), (i-2,j-2):t)
        , hmax [ (x + w k, t) | k <- [1..i - 1], let (x,t) = m ! (i - k, j) ]
        , hmax [ (x + w l, t) | l <- [1..j - 1], let (x,t) = m ! (i, j - l) ]
        ]
    hmax = maximumBy (compare `on` fst)
    w i = -i

-- alignTokens :: [String] -> [String] -> Alignment
-- alignTokens ws1 ws2 = let (s1, al1) = unwordsWithAlignment ws1
--                           (s2, al2) = unwordsWithAlignment ws2
--                       in al1 <> align s1 s2 <> reverseAlignment al2


-- | like the function unwords but return an alignment between the initial
-- list of words and the characters in the string
unwordsWithAlignment :: [String] -> (String, Alignment)
unwordsWithAlignment ss = second fromList $ getWWA $ mconcat $ map makeWWA ss
  where
    makeWWA s = WWA (s, [ (0,i) | i <- [0..length s - 1] ] )

newtype WordWithAlignment = WWA { getWWA :: (String, [(Int, Int)]) }
  deriving Eq

instance Monoid WordWithAlignment where
    mempty = WWA ("", [])
    mappend a b | a == mempty = b
                | b == mempty = a
    mappend (WWA (sa, ala)) (WWA (sb, alb)) =
        WWA (sa ++ " " ++ sb, ala ++ [ (i + 1, j + length sa + 1) | (i,j) <- alb ])

