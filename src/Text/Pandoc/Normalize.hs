module Text.Pandoc.Normalize where

import Data.Data
import Data.Generics (everywhere, mkT)
import Text.Pandoc.Definition

normalize :: Data a => a -> a
normalize = everywhere (mkT normalize')
  where
    normalize' :: [Inline] -> [Inline]
    normalize' [] = []
    normalize' (Str "":is) = is
    normalize' (Str " ":is) = Space:is
    normalize' (Str s : Str s' : is) = Str (s ++ s') : is
    normalize' (Emph []:is) = is
    normalize' (Strikeout []:is) = is
    normalize' (Subscript []:is) = is
    normalize' is = is
