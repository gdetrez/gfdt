module Data.Alignment where

import Data.List (sort, nub)
import Data.Monoid

import Data.IntMap (IntMap, fromSet, keys, findWithDefault)
import Data.IntSet (IntSet)
import qualified Data.IntSet as IntSet

data Alignment
    = FiniteAlignment (IntMap IntSet)
    | FullAlignment -- Represent the infinite alignment [(1,1),(2,2)...]
                    -- which is used as the identity in the Monoid instance.
  deriving (Eq)

instance Show Alignment where
    show FullAlignment = "0-0 1-1 ..."
    show a = case [show x ++ "-" ++ show y | (x,y) <- toList a ] of
        [] -> "∅"
        s -> unwords s

instance Monoid Alignment where
    mempty = FullAlignment
    mappend FullAlignment a = a
    mappend a FullAlignment = a
    mappend x@(FiniteAlignment mx) y =
        Data.Alignment.fromList
            [(kx,vy) | kx <- keys mx, ky <- x ! kx, vy <- y ! ky]


fromList :: [(Int, Int)] -> Alignment
fromList l = FiniteAlignment (fromSet get mkeys)
  where
    get x = IntSet.fromList (map snd (filter ((== x) . fst) l))
    mkeys = IntSet.fromList (map fst l)

toList :: Alignment -> [(Int,Int)]
toList al@(FiniteAlignment m) = [ (k,v) | k <- keys m, v <- al ! k ]
toList FullAlignment = [ (x,x) | x <- [0..] ]

type Range = (Int, Int)

(!) :: Alignment -> Int -> [Int]
(!) (FiniteAlignment m) k = IntSet.toList (findWithDefault IntSet.empty k m)
(!) FullAlignment k = [k]

alignRange :: Alignment -> (Int, Int) -> [(Int,Int)]
alignRange alignment (a,b) = group [ (x,x) | x <- single ]
  where single = sort $ nub [ j | i <- [a..b], j <- alignment!i ]
        group ((i,j):(k,l):xs) | j +1 == k = group ((i,l):xs)
                               | otherwise = (i,j) : group ((k,l):xs)
        group xs = xs

reverseAlignment :: Alignment -> Alignment
reverseAlignment FullAlignment = FullAlignment
reverseAlignment al = fromList [ (b,a) | (a,b) <- toList al ]
