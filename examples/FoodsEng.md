that cheese is warm {#this-cheese-is-warm}
===================

that [*very* **very** ***very*** boring cheese](https://en.wikipedia.org/wiki/Hush%C3%A5llsost) is very warm

> *that **very expensive** wine is fresh*

-   that warm [fish](https://en.wikipedia.org/wiki/Fish) is delicious
-   that warm [cheese](https://en.wikipedia.org/wiki/Cheese) is boring
-   that warm [wine](https://en.wikipedia.org/wiki/Wine) is expensive

1.  that <span style="color:red">warm</span>
    <span style="color:green">expensive</span>
    <span style="color:blue">fresh</span> cheese is delicious
2.  that fish is Italian
2.  that wine is delicious

**that** fish **is** very fresh
:   that fresh warm cheese is fresh

**that** cheese **is** warm
:   that wine is expensive

  that fish is Italian
  ----------------------
  that fish is <span style="color:gray">boring</span>
  that fish is <span style="color:yellow">delicious</span>
  that fish is <span style="color:green">expensive</span>
  that fish is <span style="color:blue">fresh</span>
