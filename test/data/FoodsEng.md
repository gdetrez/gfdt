that cheese is warm {#this-cheese-is-warm}
===================

that very very boring cheese is very warm

> that very expensive fresh wine is very warm

-   that fish is expensive
-   that warm cheese is boring

1.  that warm expensive cheese is expensive
2.  that fish is Italian
2.  that wine is delicious

----

that fish is very fresh
:   that fresh warm cheese is fresh

that cheese is warm
:   that wine is expensive

  that fish is Italian
  ----------------------
  that fish is boring
  that fish is delicious
  that fish is expensive
  that fish is fresh

