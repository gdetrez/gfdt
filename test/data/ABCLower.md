a b c
=====

a a b b c c
-----------

### a a a b b b c c c

a a a a
b b b b
c c c c

-   a b c
-   a a b b c c
-   a a a b b b c c c

1.  a b c
2.  a b c
3.  a b c
