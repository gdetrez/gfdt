module Text.Pandoc.NormalizeSpec where

import Test.Hspec
import Test.QuickCheck
import Text.Pandoc.Definition

import Text.Pandoc.Normalize
import Text.Pandoc.Arbitrary

spec :: Spec
spec = do
  describe "normalize" $ do
    let foobar = Str "foobar"
        foo = Str "foo"
        bar = Str "bar"
    it "concatenates a sequence of Str" $
      normalize [Str "f", Str "oo", Str "!"] `shouldBe` [Str "foo!"]
    it "removes empty Strs" $
      normalize [foo, Str "", bar] `shouldBe` [foobar]
    it "replaces literal spaces by Space" $
      normalize [foo, Str " ", bar] `shouldBe` [Str "foo", Space, Str "bar"]
    it "removes empty Emph elements" $
      normalize [foo, Emph [], bar] `shouldBe` [foobar]
    it "removes empty Strikeout elements" $
      normalize [foo, Strikeout [], bar] `shouldBe` [foobar]
    it "removes empty Subscript elements" $
      normalize [foo, Subscript [], bar] `shouldBe` [foobar]
    it "normalize reccursively" $ property $
      forAllShrink genInlines shrink $ \inlines ->
        not (null (normalize inlines)) ==>
          normalize [Emph inlines] === [Emph (normalize inlines)]

    it "is idempotent" $ property $
      forAllShrink genInlines shrink $ \inlines ->
        normalize inlines === normalize (normalize inlines)
