module Text.Pandoc.Arbitrary where

import Control.Monad
import Data.List (intersperse)
import Test.QuickCheck
import Text.Pandoc.Definition

-- | Generates a token, i.e. a non-empty string containing no spaces
genToken :: Gen String
genToken = listOf1 (elements "abcdefghijklmnopqrstuvwxyz")

-- | Generates a list of inlines where elements are separated with spaces
genInlines :: Gen [Inline]
genInlines = do
    inlines <- arbitrary
    return (intersperse Space inlines)

-- | Generate a space separated list of plain tokens
genStrs :: Gen [Inline]
genStrs = do
    strs <- listOf (liftM Str genToken)
    return (intersperse Space strs)

instance Arbitrary Inline where
    arbitrary = sized $ \s -> do
        if s == 0
        then liftM Str genToken
        else oneof
            [ liftM Str genToken
            , liftM Emph genStrs
            , liftM Strong genStrs
            , liftM Strikeout genStrs
            , liftM Superscript genStrs
            , liftM Subscript genStrs
            , liftM SmallCaps genStrs
            , liftM2 Quoted (oneof [return SingleQuote, return DoubleQuote]) genStrs
            , liftM (Cite []) genStrs
            , liftM2 Link genStrs arbitrary
            , liftM (Span ([],[],[])) genStrs
            -- , liftM2 Code arbitrary arbitrary
            --, liftM (Math InlineMath) arbitrary
            --, liftM (RawInline (Format "foo")) arbitrary
            -- , liftM2 Image genStrs arbitrary
            ]
    shrink inline = case inline of
        Str s               -> map Str (shrink s)
        Emph inlines        -> f Emph inlines
        Strong inlines      -> f Strong inlines
        Strikeout inlines   -> f Strikeout inlines
        Superscript inlines -> f Superscript inlines
        Subscript inlines   -> f Subscript inlines
        SmallCaps inlines   -> f SmallCaps inlines
        Quoted s inlines    -> f (Quoted s) inlines
        Cite cs inlines     -> f (Cite cs) inlines
        Span style inlines  -> f (Span style) inlines
        _                   -> []
      where f cons inlines = inlines ++ map cons (shrink inlines)
