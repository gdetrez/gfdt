module Text.Pandoc.ArbitrarySpec where

import Test.Hspec
import Test.QuickCheck
import Text.Pandoc.Definition

import Text.Pandoc.Arbitrary ()

spec :: Spec
spec = do
  describe "shrinkInline" $ do
    it "Shrinks a string in an Str" $
      shrink (Str "ab") `shouldMatchList` [Str "", Str "a", Str "b", Str "aa"]

