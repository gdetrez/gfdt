{-# LANGUAGE ScopedTypeVariables #-}
module Data.AlignmentsSpec where

import Data.Monoid

import Test.QuickCheck
import Test.Hspec

import Data.Alignment
import Data.Alignment.Arbitrary ()

spec :: Spec
spec = do
  describe "Alignment Monoid" $ do
    describe "mempty" $ do
      it "is a left identity" $ property $ \(x :: Alignment) ->
        mempty <> x == x

      it "is a right identity" $ property $ \(x :: Alignment) ->
        x <> mempty == x

    describe "mappend" $ do
      it "is associative" $ property $ \(x :: Alignment) y z ->
        (x <> y) <> z `shouldBe` x <> (y <> z)

      it "1-1 2-2 <> 1-1 2-2 == 1-1 2-2" $
        let a = fromList [(1,1), (2,2)] in a <> a `shouldBe` a

      it "1-2 <> 2-3 == 1-3" $
        fromList [(1,2)] <> fromList [(2,3)] `shouldBe` fromList [(1,3)]

      it "1-1 <> 2-2 == []" $
        fromList [(1,1)] <> fromList [(2,2)] `shouldBe` fromList []

    describe "fromList []" $ do
      it "is a left zero for mappend" $ property $ \x ->
        fromList [] <> x === fromList []
      it "is a right zero for mappend" $ property $ \x ->
        x <> fromList [] === fromList []

  describe "!" $ do
    it "returns a singleton with mempty" $ property $ \x ->
      mempty ! x === [x]

    it "returns [2,3] a set of values with 1-2 1-3 2-1" $
      let a = fromList [(1,2),(1,3),(2,1)]
      in a ! 1 `shouldBe` [2,3]

  describe "fromList . toList" $ do
    it "should be identity" $ property $ \al ->
      al /= mempty ==> fromList (toList al) === al

  describe "show" $ do
    it "returns 0-0 1-1 ... on mempty" $
      show (mempty :: Alignment) `shouldBe` "0-0 1-1 ..."

    it "returns 0-1 1-0 on fromList [(0,1),(1,0)]" $
      show (fromList [(0,1),(1,0)]) `shouldBe` "0-1 1-0"

    it "returns ∅ for fromList []" $
      show (fromList []) `shouldBe` "∅"

  describe "reverse" $ do
    it "should be identity if applied twice" $ property $ \al ->
      reverseAlignment (reverseAlignment al) `shouldBe` al

    it "returns 1-3 2-1 3-2 for 1-2 2-3 3-1" $
      reverseAlignment (fromList [(1,2),(2,3),(3,1)])
          `shouldBe` fromList [(1,3),(2,1),(3,2)]

  describe "alignRange" $ do
    it "return the same range with mempty" $
      alignRange mempty (2,6) `shouldBe` [(2,6)]
    it "returns a shifted range for a shifting alignment" $
      alignRange (fromList [(1,2), (2,3), (3,4)]) (1,2) `shouldBe` [(2,3)]
    it "returns a split range for a splitting alignment" $
      let alignment = fromList [(1,1), (2,3)]
      in alignRange alignment (1,2) `shouldBe` [(1,1), (3,3)]
