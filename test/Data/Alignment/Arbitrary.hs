{-# LANGUAGE LambdaCase #-}
module Data.Alignment.Arbitrary where

import Control.Monad
import Data.Monoid

import Test.QuickCheck

import Data.Alignment

genAlignment :: Gen Alignment
genAlignment
    = sized $ \case
        0 -> return mempty
        i -> resize (i-1) $ liftM fromList (listOf genPair)
  where
    genPair = liftM2 (,) genIndice genIndice
    genIndice = liftM getNonNegative arbitrary

instance Arbitrary Alignment where
    arbitrary = genAlignment
