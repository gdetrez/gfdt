module SmithWatermanSpec where

import Control.Monad (forM_)

import Test.Hspec

import Data.Alignment
import SmithWaterman -- SUT

spec :: Spec
spec = do
  describe "similarity function" $ do
    let tdata = [ (4    , "foo"   , "foo")
                , (-1   , "foo"   , "bar") ]
    forM_ tdata $ \(expected, t1, t2) ->
      it ("returns " ++ show expected ++ " for " ++ show (t1, t2)) $
          sim t1 t2 `shouldBe` expected

  describe "align" $ do
    let tdata = [ ( "", "", fromList [] )
                , ( "abcd", "efgh", fromList [] )
                , ( "abcd", "abcd", fromList [(0,0),(1,1),(2,2),(3,3)] )
                , ( "abc", "ab", fromList [(0,0),(1,1)] )
                , ( "abc", "ac", fromList [(0,0),(2,1)] )
                , ( "abc", "bc", fromList [(1,0),(2,1)] )
                , ( "foobar", "foo bar"
                  , fromList [(0,0),(1,1),(2,2),(3,4),(4,5),(5,6)] )
                ]
    forM_ tdata $ \(s1, s2, expected) ->
      it ("returns " ++ show expected ++ " when aligning " ++ show s1
          ++ " with " ++ show s2) $
          align s1 s2 `shouldBe` expected

  describe "unwordsWithAlignment" $ do
    let tdata = [ ( ["foo"], "foo", fromList [(0,0),(0,1),(0,2)] )
                , ( ["foo", "bar"], "foo bar", fromList [(0,0),(0,1),(0,2),(1,4),(1,5),(1,6)] ) ]
    forM_ tdata $ \(ts, s, al) ->
      it ("returns " ++ show (s, al) ++ " on " ++ show ts) $
        unwordsWithAlignment ts `shouldBe` (s, al)
