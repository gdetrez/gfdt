module DestyleSpec where

import Control.Monad
import Data.Monoid (mempty)
import Test.Hspec
import Test.QuickCheck
import Text.Pandoc.Builder hiding (fromList)

import Data.Alignment (fromList)
import Destyle -- SUT
import Text.Pandoc.Arbitrary
import Text.Pandoc.Normalize

genNormalizedString :: Gen String
genNormalizedString = liftM (unwords . words) arbitrary


spec :: Spec
-- spec = return ()
spec = do
  describe "destyle" $ do
    it "return an unformatted string indentically" $ property $
      forAll genNormalizedString $ \s ->
          tokens (tokenize (toList (text s))) `shouldBe` s
    it "handles initial/final spaces" $
      tokens (tokenize [Space, Str "foo", Space]) `shouldBe` " foo "
  describe "restyle" $ do
    it "apply an emphasis on a single token" $
      restyle (AT "foo" [(0,2, Emph)]) `shouldBe` [Emph [Str "foo"]]
    it "apply an emphasis on a single token in a middle of two other tokens" $
      restyle (AT "foo bar baz" [(4,6, Emph)])
          `shouldBe` [Str "foo", Space, Emph [Str "bar"], Space, Str "baz"]
    it "applies an emphasis on a patial token" $
      restyle (AT "foobar" [(3,5,Emph)])
          `shouldBe` [Str "foo", Emph [Str "bar"]]

  describe "restyle . destyle" $ do
    it "should be the indentity for Emph \"a b c\"" $
      let inlines = [Emph [Str "a", Space, Str "b", Space, Str "c"]]
      in restyle (tokenize inlines) `shouldBe` inlines

    it "should be the identity on [Inline]" $ property $
      forAllShrink genInlines shrink $ \inlines ->
          normalize inlines === restyle (tokenize inlines)
  describe "toInlineWithIndices" $ do
    it "should return a list of the same length as the input string" $ property $
      \s -> length (toInlineWithIndices s) === length s
    it "should set the first index to 0" $
      toInlineWithIndices "f" `shouldBe` [(0,Str "f")]
    it "should correctly set the subsequent indices" $
      toInlineWithIndices "f o"
          `shouldBe` [(0, Str "f"), (1, Space), (2, Str "o")]
    it "should correctly handle multiple spaces" $
      toInlineWithIndices "f  o"
          `shouldBe` [(0, Str "f"), (1, Space), (2, Space), (3, Str "o")]

  describe "transferAnnotatedTokens" $ do
    let mkAnnotatedString s a = AT s a :: AnnotatedTokens String

    it "should return the given string (annotated)" $ do
      let input = mkAnnotatedString "foo" []
          expected = mkAnnotatedString "bar" []
      transferAnnotatedTokens input "bar" mempty `shouldBe` expected

    it "returns the same list of annotation with an empty alignment" $
      let input = mkAnnotatedString "foo" [(0,0,"bla"), (2,2, "blu")]
          expected = input { tokens = "bar" }
      in transferAnnotatedTokens input "bar" mempty `shouldBe` expected

    it "inverses the annotating with an inversed alignment" $
      let input = mkAnnotatedString "foo" [(0,0,"bla"), (1,2,"bli")]
          expected = [(0,1,"bli"), (2,2,"bla")]
          alignment = fromList [(0,2), (1,1), (2,0)]
      in annotations (transferAnnotatedTokens input "oof" alignment)
          `shouldMatchList` expected

    it "returns correct annotations with a (somewhat) complex alignment" $
      let input = mkAnnotatedString "abcd" [(0,2,"bla"), (1,1,"bli"), (3,3,"blu")]
          expected = [(0,1,"bla"), (0,0,"bli"), (2,2,"blu"), (3,3,"bla")]
          alignment = fromList [(0,1),(1,0),(2,3),(3,2)]
      in annotations (transferAnnotatedTokens input "BADC" alignment)
          `shouldMatchList` expected
