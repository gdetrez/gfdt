abstract ABC = {
  flags startcat = N;

  cat N;
  fun
    n : N -> N;
    e : N;
};
