concrete ABCUpper of ABC = {
  lincat N = { a: Str ; b: Str ; c: Str ; s: Str };
  lin e = { a = "" ; b = "" ; c = "" ; s = "" };
      n x = let a' = x.a ++ "A"
            in let b' = x.b ++ "B"
            in let c' = x.c ++ "C"
            in { a = a' ; b = b' ; c = c' ; s = a' ++ b' ++ c' };
}

