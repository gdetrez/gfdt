concrete ABCLower of ABC = {
  lincat N = { a: Str ; b: Str ; c: Str ; s: Str };
  lin e = { a = "" ; b = "" ; c = "" ; s = "" };
      n x = let a' = x.a ++ "a"
            in let b' = x.b ++ "b"
            in let c' = x.c ++ "c"
            in { a = a' ; b = b' ; c = c' ; s = a' ++ b' ++ c' };
}

