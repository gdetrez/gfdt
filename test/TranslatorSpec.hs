{-# LANGUAGE OverloadedStrings, TypeSynonymInstances, FlexibleInstances #-}
module TranslatorSpec where

import Data.Monoid (mempty)
import Data.String

import Test.Hspec
import Text.Pandoc.Builder hiding (fromList)

import Data.Alignment
import Translator -- SUT

instance IsString Blocks where
    fromString = plain . text

spec :: Spec
spec = do
  describe "translate" $ do
    -- Here we define two simple "translators" for testing purposes.
    -- The first one just return the original string with an empty alignment
    -- (an empty alignment is equivalent to 1-1 2-2 3-3 ...)
    let idTranslator x = (x, mempty)
    -- The second reverse the characters in the string and gives the
    -- corresponding reversed allignment.
        revTranslator x =
            let align = fromList (zip [0..length x-1] (reverse [0..length x-1]))
            in (reverse x, align)

    it "translate a simple, unformated, text" $ do
      translate revTranslator (doc "A B C") `shouldBe` doc "C B A"

    context "Block" $ do
      it "translate a plain block" $ do
        translate revTranslator (plain "A B C") `shouldBe` plain "C B A"
      it "translate a paragraph" $ do
        translate revTranslator (para "A B C") `shouldBe` para "C B A"
      it "translate a blockquote" $ do
        translate revTranslator (blockQuote "A B C") `shouldBe` blockQuote "C B A"
      it "translate a bullet list" $ do
        translate revTranslator (bulletList ["A B C"])
            `shouldBe` bulletList ["C B A"]
      it "translates items in a definition list" $ do
        translate revTranslator (definitionList [ ( "foo", ["bar"] ) ])
            `shouldBe` definitionList [ ("oof", ["rab"]) ]
      it "translate a header" $ do
        translate revTranslator (header 1 "abc") `shouldBe` header 1 "cba"
      it "translate a table" $ do
        translate revTranslator (table "cap" [(AlignLeft, 1)] ["foo"] [["bar"]])
          `shouldBe` table "pac" [(AlignLeft, 1)] ["oof"] [["rab"]]

      context "with identity translator" $ do
        it "translate a block with emphase" $
          translate idTranslator (plain (emph "A B C"))
            `shouldBe` plain (emph "A B C")
        it "translates a block with partial emphase" $
          let block = plain (emph "A" <> text " B C")
          in translate idTranslator block `shouldBe` block

      context "with a reverse translator" $ do
        it "translate a block with emphase" $
          translate revTranslator (plain (emph "A B C"))
            `shouldBe` plain (emph "C B A")
        it "translates a block with partial emphase" $
          let block = plain (emph "A" <> text " B C")
              expected = plain (text "C B " <> emph "A")
          in translate revTranslator block `shouldBe` expected
        it "translates a block with nested styling" $
          let block = plain (emph (text "A " <> strong "B"))
              expected = plain (emph (strong "B" <> text " A"))
          in translate revTranslator block `shouldBe` expected

  describe "alignTranslation" $ do
    it "align a really simple translation with one token" $
      alignTranslation "a" (["a"],["A"],[(0,0)]) `shouldBe`
        fromList [(0,0)]

    it "align a really simple translation with two tokens" $
      alignTranslation "a b" (["a", "b"],["A", "B"],[(0,0), (1,1)])
        `shouldBe` fromList [(0,0), (2,2)]

    it "align an inversed translation with two tokens" $
      alignTranslation "a b" (["a", "b"], ["B", "A"], [(0,1),(1,0)])
        `shouldBe` fromList [(0,2),(2,0)]
