module GfdtSpec where

--import Control.Monad
import Test.Hspec
--import Text.Pandoc.Definition
--import System.FilePath

--import Gfdt -- SUT

-- TODO Fix and uncomment

-- | When given the path to a pgf file and the name of two concretes CS and CT
-- which should exist in this PGF, this function creates a test according
-- to the following pattern where data/FooBar.pgf is the pgf file, Foo the
-- source language and Bar the target language, then there should be two files
-- data/Foo.md and data/Bar.md such that gfdt is expected to return Bar.md
-- when translating Foo.md
-- mkSystemTest :: FilePath -> String -> String -> Spec
-- mkSystemTest pgf source target =
--     it (source ++ " →  " ++ target) $ do
--         let pgf_dir = takeDirectory pgf
--             inputf  = pgf_dir </> source <.> "md"
--             goldf   = pgf_dir </> target <.> "md"
--         gold <- liftM getMarkdown (readFile goldf)
--         liftM getMarkdown (gfdt ["-p", pgf, "-f", source, "-t", target, inputf])
--           `shouldReturn` gold

-- | Parse markdown. It's less fragile to compare parsed markdown files
-- than the strings
-- getMarkdown :: String -> Pandoc
-- getMarkdown = readMarkdown def

spec :: Spec
spec = return () -- do
--     context "with ABC.pgf" $ do
--         mkSystemTest  "test/data/ABC.pgf" "ABCUpper" "ABCLower"
--         mkSystemTest  "test/data/ABC.pgf" "ABCLower" "ABCUpper"
--     context "with Foods.pgf" $ do
--         mkSystemTest  "test/data/Foods.pgf" "FoodsFre" "FoodsEng"
--         mkSystemTest  "test/data/Foods.pgf" "FoodsEng" "FoodsFre"
