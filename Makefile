testdata: test/grammars/ABC.pgf
.PHONY: testdata

test/grammars/ABC.pgf: test/grammars/ABCUpper.gf test/grammars/ABCLower.gf
	cabal exec -- gf --make -D test/grammars $^
