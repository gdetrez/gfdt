# gfdt

## Introduction

`gfdt` is a tool that allow the translation of structured documents using
the [Grammatical Framework][GF]. It'll keep
the structure of the document intact (headings, paragraphs, lists, etc) and
will do its best to transfer inlines formatting (emphasis, strong, links...)
from one language to the other.

## How does it work

![`gfdt` workflow](doc/gfdt_lowdef.png)

## Examples

You can find an example document translated into many language using `gfdt` and
the foods grammar at
[https://ci.zjyto.net/job/gfdt/examples](https://ci.zjyto.net/job/gfdt/examples)

## Usage

`gfdt` is a pandoc json filter, as such it is better used in conjunction with
[pandoc]:

```
pandoc mydocument.md -t json \
  | gfdt --pgf=Foo.pgf --from FooEng --to FooFre \
  | pandoc -f json -o mondocument.md
```

**BYOG (bring your own grammar): `gfdt` doesn't come with a grammar,
you will need to bring your own pgf or use one from the [GF repository]**

## Contributing

Open a [merge request](https://gitlab.com/gdetrez/gfdt/merge_requests) on
gitlab.

## Help

If you encounter problems with `gfdt` you can
open an [issue](https://gitlab.com/gdetrez/gfdt/issues)
or [contact me][gregoire.detrez@gu.se].
For help about GF,
try the [GF user group](https://groups.google.com/forum/#!forum/gf-dev)

## Installation

### Requirements

To build this project, you will need a fairly recent version of the
[haskell platform](http://www.haskell.org/platform/).

### Installation

For now, `gfdt` depends on a patched version of GF, so a simple `cabal install`
in the root of the repository will most likely not work. You can use the
included `scripts/bootstrap` script to install the dependencies:

```
./scripts/bootstrap
cabal build
```

## Credits

Created by Grégoire Détrez

## Contact

Contact me at [gregoire.detrez@gu.se]

## License

This project is licensed under The GNU General Public License version 3.


[GF]: http://www.grammaticalframework.org/
[GF repository]: https://github.com/GrammaticalFramework/GF/
[gregoire.detrez@gu.se]: mailto:gregoire.detrez@gu.se
[pandoc]: http://pandoc.org/
